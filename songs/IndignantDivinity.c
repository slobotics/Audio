// This file is part of Audio, and is therefore licensed under the GNU GPLv3 public license.
// Copyright (C) 2017 Ganden Schaffner

// Source: http://www.vgmusic.com/file/60daadbff54d6bdee1b7b425adc767f1.html

// Notes (in Hz)
#define a5 880
#define b5 988
#define d6 1175
#define e6 1319
#define f6 1397
#define fs6 1480
#define a6 1760
#define c7 2093
#define b6 1976
#define cs7 2217
#define d7 2349
#define e7 2637
#define fs7 2960
#define a7  3520
#define b7 3951

// Note Lengths
// Song plays at 280 BPM
// playNote takes in values in 1/100s of a sec (50 => .5s playtime)
#define bt(a) (a / 280.0 * 3000)

/*! A method to play a tone for a set duration of time.
	\param freq an integer in Hz.
	\param duration an integer in 1/100s of a second.
*/
void playNote(int freq, int duration) {
	playTone(freq, duration);
	wait10Msec(duration);
}

void aIntroMain() {
	playNote(cs7, bt(6));
	playNote(b6, bt(6));
	playNote(fs7, bt(4));
	playNote(e7, bt(4));
	playNote(a7, bt(2));
	playNote(cs7, bt(4));
	playNote(d7, bt(4));
	playNote(e7, bt(2));
}

void aIntroEnding1() {
	playNote(cs7, bt(6));
	playNote(d7, bt(6));
	playNote(cs7, bt(4));
	playNote(b6, bt(16));
}

void aIntroEnding2() {
	playNote(cs7, bt(14));
	playNote(cs7, bt(1));
	playNote(c7, bt(1)); // some odd timing to look into around here
	playNote(b7, bt(1));
	playNote(a7, bt(16));
}

void aIntroEnding3() {
	playNote(cs7, bt(2));
	playNote(d7, bt(2));
	playNote(cs7, bt(2));
	playNote(d7, bt(2));
	playNote(cs7, bt(2));
	playNote(a6, bt(2));
	playNote(fs6, bt(2));
	playNote(b6, bt(4));
	playNote(f6, bt(2));
	playNote(e6, bt(2));
	playNote(d6, bt(2));
	playNote(e6, bt(2));
	playNote(d6, bt(2));
	playNote(b5, bt(2));
	playNote(a5, bt(2));
}

task pSongIndignantDivinity() {
	aIntroMain();
	aIntroEnding1();
	aIntroMain();
	aIntroEnding2();
	aIntroMain();
	aIntroEnding1();
	aIntroMain();
	aIntroEnding3();
	aIntroMain();
	aIntroEnding1();
	aIntroMain();
	aIntroEnding2();
	// This is when lower notes start
	aIntroMain();
	aIntroEnding1();

}
////////////////////////////////////////////DO DEFINES GO OUTSIDE OF THEIR FILE? UNDEF?
// Look into playImmediateTone for overlapping tones?
// Can tones be queued? (see playImmediateTone hovertext)
// WINDOW -> RING TONE CONVERTER! Can a multi-track MIDI be converted to RTTTL?
