# Audio
A RobotC submodule that includes a collection of songs, many generated using [MIDI-to-RobotC](https://gitlab.com/slobotics/MIDI-to-RobotC).

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
